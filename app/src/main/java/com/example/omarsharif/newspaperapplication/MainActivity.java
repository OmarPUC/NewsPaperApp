package com.example.omarsharif.newspaperapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImageButton btn1 = findViewById(R.id.daily1);
        ImageButton btn2 = findViewById(R.id.daily2);
        ImageButton btn3 = findViewById(R.id.daily3);
        ImageButton btn4 = findViewById(R.id.daily4);
        ImageButton btn5 = findViewById(R.id.daily5);

        btn1.setOnClickListener(this);
        btn2.setOnClickListener(this);
        btn3.setOnClickListener(this);
        btn4.setOnClickListener(this);
        btn5.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.daily1:
                Intent intent = new Intent(this, Main2Activity.class);
                intent.putExtra("url", "https://www.youtube.com");
                startActivity(intent);
                break;

            case R.id.daily2:
                intent = new Intent(this, Main2Activity.class);
                intent.putExtra("url", "http://www.prothomalo.com");
                startActivity(intent);
                break;

            case R.id.daily3:
                intent = new Intent(this, Main2Activity.class);
                intent.putExtra("url", "http://www.ittefaq.com.bd");
                startActivity(intent);
                break;
        }
    }
}
